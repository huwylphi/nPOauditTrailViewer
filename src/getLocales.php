<?php

/*
 * version: 1.5
 * date: 2017-01-11
 * developer: Ph. Huwyler

 * URL/ARG PARAMETERS:
 *
 * path (optional):	the path where the localization file are located
 *
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// Get path from URL
$path = "./";
if (array_key_exists('path', $_GET)) {
    $path = $_GET['path'];
    if (!(substr($path, -strlen('/')) === '/')) {
        $path = $path."/";
    }
}

$list = glob($path."*.json");
sort($list);
foreach ($list as $l) {
    if (preg_match('/^[a-z]{2}$/i', basename($l, ".json"))) {
        $files[strtolower(basename($l, ".json"))] = $l;
    }
}

echo json_encode($files);
?>