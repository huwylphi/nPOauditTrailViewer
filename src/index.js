/* 
 * version: 1.8
 * date: 2022-05-17
 * developer: Ph. Huwyler
 
 * URL PARAMS:
 * - dynFilterList: ex. dynFilterList=%5B"TagDescription%20LIKE%20%27%25UK3%25%27"%5D (like SQL filter but use url encoder. You can use an online url encoder like http://www.url-encode-decode.com/)
 * - startDate and endDate: ex. startDate=2015-06-01 or endDate=-7 in absolute or in relative format
 * - autoStart: ex. autoStart=true
 * - pageLen: ex. pageLen=50
 * - colSort: ex. colSort=-5 or colSort=-0. sort by col index
 * - top: ec. top=25. the top x items
 */

$(document).ready(function() {
    // helper function to get url params
    $.urlParam = function(name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results==null) {
            return null;
        } else {
            return decodeURIComponent(results[1]) || 0;
        }
    }

    $.setCookie = function(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    $.getCookie = function(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    
    // get all possible locales for localization
	$.getJSON("./getLocales.php?path=./inc/localization", function(localesArray) {
        $.i18n().load(localesArray)
        .done(function() {
            console.log("reading localizations");
            var localeCount = 0;
            $.each(localesArray, function(locale) {
                localeCount++;
                var a = "<a href=\"#\" data-locale=\"" + locale + "\">" + locale.toUpperCase() + "</a>";
                if (localeCount < Object.keys(localesArray).length) {
                    a = a + "&nbsp;|&nbsp;"
                }
                $(".switch-locale").append(a);
            });

            $('.switch-locale').on('click', 'a', function(e) {
                e.preventDefault();
                set_locale_to($(this).data('locale'), null);
            });

            // get locale from cookie
            var l = $.getCookie('locale');
            if(!l) {
                l = 'en';   // default locale = 'en'
            }
            // set locale on site load
            set_locale_to(l, function() {
                $('#period').css("visibility", "visible");
                $('#header').css("visibility", "visible");
                $.getJSON("./config.json", function(config) {
                    var table;
                    var firstLoad = true;
                    $("#go").click(function(event) {
                        // display spinner
                        $('#spinner').css("visibility", "visible");
                        $('#dtw').css("visibility", "hidden");
                        $("#info").html($.i18n('loading-data'));

                        var req = './getData.php?';
                        if($("#period-from").datepicker('getDate')) {
                            req = req + 'startDate=' + $.format.date($("#period-from").datepicker('getDate'), "yyyy-MM-dd");
                        }
                        if($("#period-to").datepicker('getDate')) {
                            req = req + '&endDate=' + $.format.date($("#period-to").datepicker('getDate'), "yyyy-MM-dd");
                        }
                        
                        if($.urlParam('dynFilterList') != null) {
                            req = req + "&dynFilterList=" + $.urlParam('dynFilterList');
                        }
                        
                        if($.urlParam('top') != null) {
                            req = req + "&top=" + $.urlParam('top');
                        }

                        if(config.debug) {
                            console.log("getData.php ajax request:" + req);
                        }

                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: req,
                            timeout: config.ajax.timeout,
                            success: function(json) {
                                $("#info").html($.i18n('processing-data'));
                                // reset table on each go click
                                if(table) {
                                    table.destroy();
                                }
                                $('thead').empty();
                                $('tbody').empty();
                                $('tfoot').empty();

                                if(json.hasOwnProperty('exception')) {
                                    $('#info').html(json.exception);
                                    $('#spinner').css("visibility", "hidden");
                                } else {
                                    var columnCaptions4dt = [];
                                    // feed table with new data
                                    if(json.data.length > 0) {
                                        // generate columns for DataTable
                                        var tr = $('<tr></tr>');
                                        tr.appendTo($('thead'));
                                        $.each(json.data[0], function(a, b) {
                                            $('<th class="' + a + '">' + a + '</th>').appendTo(tr);

                                            // generate columns param for DataTable object as array
                                            var colCaption = $.i18n('dataTable-columnCaption-' + a).replace('dataTable-columnCaption-', '');
                                            columnCaptions4dt.push($.parseJSON('{"data": "' + a + '", "title":"' + colCaption + '"}'));
                                        });

                                        // generate rows for DataTable
                                        $.each(json.data, function(a, b) {
                                            var tr = $('<tr></tr>').appendTo($('tbody'));
                                            $.each(b, function(i, j) {
                                                $('<td>' + j + '</td>').appendTo(tr);
                                            });
                                        });

                                        // rebuild the DataTable footer with inputs for single columns search functionality
                                        tr = $('<tr></tr>');
                                        tr.appendTo($('tfoot'));
                                        $.each(json.data[0], function(a, b) {
                                            $('<td class="' + a + '">' + $.i18n('dataTable-columnCaption-' + a).replace('dataTable-columnCaption-', '') + '</td>').appendTo(tr);
                                        });
                                        // replace footer th text with search inputs
                                        $('#dt tfoot td').each(function() {
                                            var title = $(this).text();
                                            $(this).html('<input type="search" class="form-control" placeholder="' + $.i18n('dataTable-filter-columns-search') + ' ' + title + '" />');
                                        });
                                    } else {
                                        $('#info').html($.i18n('nodata'));
                                        $('#spinner').css("visibility", "hidden");
                                    }

                                    // new DataTable()
                                    $(document).on('preInit.dt', function(e, settings) {
                                        if(!firstLoad) { return; }

                                        var api = new $.fn.dataTable.Api(settings);

                                        // page len from url
                                        if($.urlParam('pageLen') != null) {
                                            try {
                                                api.page.len(parseInt($.urlParam('pageLen')));
                                            } catch (err) {throw err;}
                                        }

                                        // col sort from url
                                        if($.urlParam('colSort') != null) {
                                            try {
                                                api.order([Math.abs(parseInt($.urlParam('colSort'))), $.urlParam('colSort').indexOf('-') > -1 ? 'desc' : 'asc']);
                                            } catch (err) {throw err;}
                                        }
                                    });

                                    // set table caption
                                    var caption	= $.i18n('app-title') + ": "
                                                + $.format.date($("#period-from").datepicker('getDate'), "yyyy-MM-dd")
                                                + " - "
                                                + $.format.date($("#period-to").datepicker('getDate'), "yyyy-MM-dd")

                                    $('#tbl').find("caption")
                                    .text(caption)	// set table caption so it will be printable
                                    .hide();		// but hide the original table caption because it will be set in #info
                                    
                                    // build jquery DataTable options
                                    var dtOptions = {
                                        language:       {url: "./inc/localization/" + $.i18n().locale + ".json"},
                                        columns:        columnCaptions4dt,
                                        scrollY:        '60vh',
                                        scrollCollapse: true,
                                        scrollX:        true,
                                        stateSave:      true,
                                        colReorder:     true,
                                        lengthMenu:     [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                                        dom:            'Bft<"dt-footer"lpi>',
                                        search:         {caseInsensitive: config.table.search.caseInsensitive},
                                        buttons: [
                                            {extend: 'colvis',  text: $.i18n('dataTable-buttons-colvis-text')},
                                            {extend: 'print',   text: $.i18n('dataTable-buttons-print-text'), title: caption},
                                            {extend: 'csv',     text: 'CSV', fieldSeparator: ';', filename: caption},
                                            {extend: 'copy',    text: $.i18n('dataTable-buttons-copy-text')},
                                            {extend: 'excel',   text: 'EXCEL', filename: caption},
                                            {extend: 'pdf',     text: 'PDF', title: caption, orientation: 'landscape', pageSize: 'A2', filename: caption}
                                        ]
                                    }
                                    // override buttons default parameters with data from config.export
                                    $.each(dtOptions.buttons, function(a, b) {
                                        if(config.table.export.hasOwnProperty(b.extend)) {
                                            $.each(config.table.export[b.extend], function(i, j) {
                                                b[i] = j;
                                            });
                                        }
                                    });

                                    table = $('#dt')
                                    .on('column-visibility.dt', function(e, settings, column, state) {
                                        if(firstLoad){return;}  // only save columns visibility changes after table has been loaded once
                                        $.setCookie('colVisibilityChanged', true, 3650);    // save in a cookie the info that the columns visibility has change by the user so that the initialization through config.table.colHidden will not be affected
                                    })
                                    .on( 'stateLoadParams.dt', function (e, settings, data) {
                                        // reset search filters
                                        data.search.search = "";
                                        $.each(data.columns, function(a, b) {
                                            b.search.search = "";
                                        });
                                    })
                                    .on('draw.dt', function () {
                                        
                                    })
                                    .on('init.dt', function() {
                                        // add tooltip to search filter input
                                        $('.dataTables_filter').attr({
                                            'data-toggle': 'tooltip',
                                            'title': $.i18n('dataTable-filter-tooltip')
                                        });

                                        // Apply the search on each DataTable column
                                        table.columns().every(function() {
                                            var that = this;
                                    
                                            $('input', this.footer()).on('keyup change', function() {
                                                if(that.search() !== this.value) {
                                                    that
                                                        .search(this.value)
                                                        .draw();
                                                }
                                            });
                                        });

                                        // hidden columns from config.table.colHidden (if not yet changed manually by the user)
                                        if(!($.getCookie('colVisibilityChanged') || false)) {
                                            $.each(config.table.colHidden, function(a, b) {
                                                table.columns('.' + b).visible(false);  // b = column caption. column caption has been set as class at table build
                                            });
                                        }

                                        // column reorder from config.table.colReorder
                                        var colReordered = false;
                                        $.each(table.state().ColReorder, function(a, b) {
                                            if(a != b) {        // if a == b in all array indexes, the column have not been reordered from user yet
                                                colReordered = true;
                                                return false;   // break
                                            }
                                        });
                                        if(!colReordered) {
                                            // build columns order by its indexes.
                                            var newColReorder = [];
                                            // 1. get indexes from config.table.colReorder
                                            $.each(config.table.colReorder, function(a, b) {
                                                var idx = table.columns('.' + b)[0][0];
                                                newColReorder.push(idx);
                                            });
                                            //2. add missing indexes
                                            table.columns().every(function() {
                                                var idx = this.index();
                                                if(newColReorder.indexOf(idx) < 0) {
                                                    newColReorder.push(idx);
                                                }
                                            });
                                            // reorder table columns
                                            table.colReorder.order(newColReorder);
                                        }

                                        firstLoad = false;
                                        $('#dtw').css("visibility", "visible");
                                        $('#spinner').css("visibility", "hidden");
                                    })
                                    .DataTable(dtOptions);
                                }
                                $("#info").html("");
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                if(config.debug) {
                                    console.log(errorThrown);
                                }
                                $("#info").html(errorThrown);
                                $('#spinner').css("visibility", "hidden");
                            },
                            complete: function(jqXHR, textStatus) {
                                $('#spinner').css("visibility", "hidden");  // security
                            }
                        });
                    });

                    // get autoStart param from url to check if we need to run the query on site loading
                    if($.urlParam('autoStart') != null) {
                        try {
                            if($.parseJSON($.urlParam('autoStart')) === true) {
                                $("#go").click();
                            }
                        } catch (err) {throw err;}
                    }
                });
            });
        });
    });
});

// language selection
var set_locale_to = function(locale, callback) {
    // save locale in a cookie for 1 year
    $.setCookie('locale', locale, 365);

	if (locale) {
		$.i18n().locale = locale;
	}

	$('body').i18n();

	if(!(locale === "en")) {
		$("#logo").attr("src", "inc/images/logo-" + locale + ".png");
	} else {
		$("#logo").attr("src", "inc/images/logo.png");
	}

    $.getScript( "lib/bootstrap-datepicker-1.6.4-dist/locales/bootstrap-datepicker." + $.i18n().locale + ".min.js", function( data, textStatus, jqxhr ) {
        initDatepickers();
        if(typeof callback === "function") {
            callback();
        }
    });
};

// datepicker initialization
var initDatepickers = function() {
    // save current date to restitute the date after destroying it
    var periodFromDate = $("#period-from").datepicker('getDate');
    var periodToDate = $("#period-to").datepicker('getDate');

    var td = new Date();
    sd = new Date(td.getFullYear(), td.getMonth(), td.getDate(), 0, 0, 0, 0);
    ed = new Date(td.getFullYear(), td.getMonth(), td.getDate(), 23, 59, 59, 999);

    // init from - to fields as datepicker
    $('.input-daterange input').each(function() {
        $(this).datepicker('destroy');  // we have to destroy the datepicker to set the new language (we do not have any method to set language options)

        $(this).datepicker({
            format: 'dd/mm/yyyy',
            language: $.i18n().locale,
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            todayBtn: true
        }).on('changeDate', function(e) {
            // manage min and max possible date to ensure to date is always bigger than from date but not bigger as today
            switch (e.currentTarget.id) {
            case "period-from":
                $("#period-to").datepicker('setStartDate', e.date);
                break;
            case "period-to":
                $("#period-from").datepicker('setEndDate', e.date);
                break;
            }
        });
        $(this).datepicker('setDate', $(this)[0].id === "period-from" ? sd : ed);
        $(this).datepicker('setEndDate', $(this)[0].id === "period-from" ? sd : ed);
    });

    // init datepicker with date of today or from url params startDate and endDate (in absolute or relative format)
    if(periodFromDate) {
        $("#period-from").datepicker('setDate', periodFromDate);
    } else {
        if($.urlParam('startDate') != null) {
            if((new RegExp(/^\-{1}\d+$/)).test($.urlParam('startDate')) == true) {
                var d = new Date();
                d.setDate(d.getDate()-Math.abs(parseInt($.urlParam('startDate'))));
                $("#period-from").datepicker('setDate', d);
            } else if((new RegExp(/\b\d{4}[-]\d{1,2}[-]\d{1,2}\b/)).test($.urlParam('startDate')) == true) {
                $("#period-from").datepicker('setDate', new Date($.urlParam('startDate')));
            }
        } else if($.urlParam('top') != null) {	// case startDate not defined and top is defined, set startDate to first possible date (otherwise startDate would be today or given params)
            $("#period-from").datepicker('setDate', new Date('1970-01-01'));
        }
    }

    if(periodToDate) {
        $("#period-to").datepicker('setDate', periodToDate);
    } else {
        if($.urlParam('endDate') != null) {
            if((new RegExp(/^\-{1}\d+$/)).test($.urlParam('endDate')) == true) {
                var d = new Date();
                d.setDate(d.getDate()-Math.abs(parseInt($.urlParam('endDate'))));
                $( "#period-to" ).datepicker('setDate', d);
            } else if((new RegExp(/\b\d{4}[-]\d{1,2}[-]\d{1,2}\b/)).test($.urlParam('endDate')) == true) {
                $("#period-to").datepicker('setDate', new Date($.urlParam('endDate')));
            }
        }
    }
};