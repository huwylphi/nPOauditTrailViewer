<?php

/*
 * version: 2.1
 * date: 2022-06-13
 * developer: Ph. Huwyler

 * URL/ARG PARAMETERS:
 *
 * startDate (optional):	start period of report, default is 1970-01-01
 * endDate (optional):		end period of report, default is now
 * dynFilterList (optional):an AND SQL filter url encoded
 * top (optional):			select the top x items
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

$startTimestamp = "'1970-01-01 00:00'";			// default start period = begin of Unix timestamps
$endTimestamp = "'".date("Y-m-d H:i:s")."'";	// default end period = now

$CMDARG = array();
if(isset($argv)) {
	parse_str(implode('&', array_slice($argv, 1)), $CMDARG);
}

// Read config file
$config_json = file_get_contents("config.json");
$config=json_decode($config_json);

// Get startDate from URL
if (array_key_exists('startDate', $_GET)) {
	$startTimestamp = "'".$_GET['startDate']." 00:00'";
} else if (array_key_exists('startDate', $CMDARG)) {
	$startTimestamp = $CMDARG['startDate'];
}
if ($config->{'debug'} && isset($startTimestamp)) { error_log("startDate: ".$startTimestamp); }

// Get endDate from URL
if (array_key_exists('endDate', $_GET)) {
	$endTimestamp = "'".$_GET['endDate']." 23:59:59'";
} else if (array_key_exists('endDate', $CMDARG)) {
	$endTimestamp = $CMDARG['endDate'];
}
if ($config->{'debug'} && isset($endTimestamp)) { error_log("endDate: ".$endTimestamp); }

// Get dynFilterList from URL
if (array_key_exists('dynFilterList', $_GET)) {
	$dynFilterListParam = $_GET['dynFilterList'];
} else if (array_key_exists('dynFilterList', $CMDARG)) {
	$dynFilterListParam = $CMDARG['dynFilterList'];
}
if ($config->{'debug'} && isset($dynFilterListParam)) { error_log("dynFilterList: ".$dynFilterListParam); }

// Get top param from URL
if (array_key_exists('top', $_GET)) {
	$topParam = $_GET['top'];
} else if (array_key_exists('top', $CMDARG)) {
	$topParam = $CMDARG['top'];
}
if ($config->{'debug'} && isset($topParam)) { error_log("top: ".$topParam); }

// Connect to SQL-Server DB
$connectionInfo = array(
	"Database" => $config->{'sqlServer'}->{'DBname'},
	"UID" => $config->{'sqlServer'}->{'userName'},
	"PWD" => $config->{'sqlServer'}->{'password'},
	'CharacterSet' => 'UTF-8',	// to prevent [Microsoft][ODBC Driver 17 for SQL Server] String data, right truncation Errors on Linux, you must add the Connection Parameter 'CharacterSet' => "UTF-8"
	'ReturnDatesAsStrings' => true,
	'TrustServerCertificate' => true
);
if ($config->{'debug'}) { error_log("connectionInfo: ".$config->{'sqlServer'}->{'serverName'}." - ".json_encode($connectionInfo)); }

$conn = sqlsrv_connect(
	$config->{'sqlServer'}->{'serverName'},
	$connectionInfo
);

if($conn === FALSE) {
	die("{\"exception\":\"Connection could not be established.\", \"innerException\":".json_encode(utf8ize(sqlsrv_errors()))."}");
}

// format start and end date to the corresponding config parameter for sql query (SQL-Server date format is dependent from the SQL-Server locale language)
$date = new DateTime(str_replace("'","", $startTimestamp));
$startTimestamp = "'".$date->format($config->{'sqlServer'}->{'dateTimeFormat'})."'";

$date = new DateTime(str_replace("'","", $endTimestamp));
$endTimestamp = "'".$date->format($config->{'sqlServer'}->{'dateTimeFormat'})."'";

//Execute the SQL-query with a scrollable cursor so we can determine the number of rows returned.
$params = array(&$_POST['query']);
$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);

$arrSqlResult = array();

// Build static filter list from config file
$staticFilterList = "";
$i=0;
foreach($config->{'sqlServer'}->{'staticFilterList'} as $staticFilter) {
	if(!empty($staticFilter)) {
		if($i<=0) {
			$staticFilterList .= $staticFilter;
			$i++;
		} else {
			$staticFilterList .= " AND ".$staticFilter;
			$i++;
		}
	}
}

// Build dynamic filter list from url params
$dynFilterList = "";
if(isset($dynFilterListParam)) {
	foreach(json_decode($dynFilterListParam) as $dynFilter) {
		if(!empty($dynFilter)) {
			$dynFilterList .= " AND ".$dynFilter;
		}
	}
}

// Build top filter from url params
$top = "";
if(isset($topParam)) {
	if(is_numeric($topParam)) {
		$top = "TOP ".abs(intval($topParam));
	}
}

// Check if VfiTag DB is attached. If yes get TAG description too
$sqlQuery = "SELECT name FROM master..sysdatabases WHERE name = '".$config->{'sqlServer'}->{'VfiTagDBname'}."'";
$stmt = sqlsrv_query( $conn, $sqlQuery, $params, $cursorType);
$VfiTagDBattached = false;
if( sqlsrv_num_rows ($stmt)>0 ) {
	$VfiTagDBattached = true;
}
sqlsrv_free_stmt($stmt);
if ($config->{'debug'}) { error_log("VfiTagDBattached: ".$VfiTagDBattached); }

// Execute SQL-query
if($VfiTagDBattached) {
	$sqlQuery = "SELECT ".$top." a.* FROM
	((SELECT "
	.$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[UserName],"
	.$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[SourceType],"
	.$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[SourceName],"
    .$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[SourceID],"
    .$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[Action],"
    .$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[TagName],"
    .$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[TagValue],"
    .$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[TagStr],"
    .$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[ZoneName],"
    .$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[MacroName],"
	//.$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[TimeAction],"	// no formating: row as in db table
	."CONVERT(VARCHAR(16),".$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[TimeAction], 120) as TimeAction,"		// date formating: yyyy-mm-dd hh:mm
	//."CONVERT(VARCHAR(19),".$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[TimeAction], 120) as TimeAction,"	// date formating: yyyy-mm-dd hh:mm:ss
    .$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[UserFullName],"
    .$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[UserDescription],"
    .$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[StationName],"
    .$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[PreviousTagValue],"
    .$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[PreviousTagStr],"
    .$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[Object],"
    .$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[info1],"
    .$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[info2],
	[".$config->{'sqlServer'}->{'VfiTagDBname'}."].[dbo].[VfiTagRef].description as TagDescription
		FROM ".$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail]
		INNER JOIN [".$config->{'sqlServer'}->{'VfiTagDBname'}."].[dbo].[VfiTagRef] ON RIGHT(".$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[TagName], LEN(".$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[TagName]) - CHARINDEX(':', ".$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[TagName])) = RIGHT([".$config->{'sqlServer'}->{'VfiTagDBname'}."].[dbo].[VfiTagRef].[longName], LEN([".$config->{'sqlServer'}->{'VfiTagDBname'}."].[dbo].[VfiTagRef].[longName]) - CHARINDEX(':', [".$config->{'sqlServer'}->{'VfiTagDBname'}."].[dbo].[VfiTagRef].[longName]))
		WHERE TimeAction >= ".$startTimestamp." AND TimeAction <= ".$endTimestamp.")
	UNION
	(SELECT "
	.$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[UserName],"
	.$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[SourceType],"
	.$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[SourceName],"
    .$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[SourceID],"
    .$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[Action],"
    .$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[TagName],"
    .$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[TagValue],"
    .$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[TagStr],"
    .$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[ZoneName],"
    .$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[MacroName],"
	//.$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[TimeAction],"	// no formating: row as in db table
	."CONVERT(VARCHAR(16),".$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[TimeAction], 120) as TimeAction,"		// date formating: yyyy-mm-dd hh:mm
	//."CONVERT(VARCHAR(19),".$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[TimeAction], 120) as TimeAction,"	// date formating: yyyy-mm-dd hh:mm:ss
    .$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[UserFullName],"
    .$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[UserDescription],"
    .$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[StationName],"
    .$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[PreviousTagValue],"
    .$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[PreviousTagStr],"
    .$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[Object],"
    .$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[info1],"
    .$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[info2],
	'' as TagDescription
		FROM ".$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail]
		WHERE ".$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail].[TagName] = '' AND TimeAction >= ".$startTimestamp." AND TimeAction <= ".$endTimestamp.")) as a
	WHERE ".$staticFilterList.$dynFilterList."
	ORDER BY a.TimeAction desc";
} else {
	$sqlQuery = "SELECT ".$top." *
		FROM ".$config->{'sqlServer'}->{'DBname'}.".[dbo].[AuditTrail]
		WHERE TimeAction >= ".$startTimestamp." AND TimeAction <= ".$endTimestamp." AND ".$staticFilterList.$dynFilterList."
		ORDER BY TimeAction desc";	
}
//echo $sqlQuery; //for testing
if ($config->{'debug'}) { error_log("sqlQuery: ".$sqlQuery); }
$stmt = sqlsrv_query($conn, $sqlQuery, $params, $cursorType);

if($stmt === FALSE) {
	die("{\"exception\":\"SQL query execution failed.\", \"innerException\":".json_encode(utf8ize(sqlsrv_errors()))."}");
}

while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)) {
	array_push($arrSqlResult, $row);
}

// Free the statement and connection resources
sqlsrv_free_stmt($stmt);

// Close the connection
sqlsrv_close($conn);

function utf8ize($d) {
    if (is_array($d)) {
        foreach ($d as $k => $v) {
            $d[$k] = utf8ize($v);
        }
    } else if (is_string ($d)) {
        return utf8_encode($d);
    }
    return $d;
}

// Return result array an json string
//if ($config->{'debug'}) { error_log("sqlResponse: ".print_r($arrSqlResult)); }
echo "{\"period\":{\"from\":".json_encode(date_parse(str_replace("'","",$startTimestamp))).",\"to\":".json_encode(date_parse(str_replace("'","",$endTimestamp)))."},\"data\":".json_encode($arrSqlResult, JSON_UNESCAPED_UNICODE)."}";
?>
