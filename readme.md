# nPOauditTrailViewer
An alternativ audit trail viewer for novaPro Open
## system requirement:
- Web-server like iis and php with sqlsrv extensions activated
- Microsoft ODBC Driver for SQL Server
- or Docker engine
- MS SQL-Server with nPO AuditTrail database attached (optionally attach nPO VfiTag database too)
- optional: Adobe flash-player for file-export from html report web-site (obsolete)

## tested environment:
- Windows Server 2008 R2 with IIS, Windows Server 2012 R2 with IIS
- PHP 5.3, 5.4, 5.5, 8.1 with sqlsrv extensions activated
- MS SQL-Server 2008 R2 Express edition 2005, 2012, 2014, 2016, 2019
- MS IE9, IE10, IE11, Google Chrome >42, Mozilla Firefox >35

## setup:
1. unzip the file in to a folder and integrate web-app in to iis  
or  
2. pull docker image (_docker pull registry.gitlab.com/huwylphi/npoaudittrailviewer:latest_) or build docker image locally from source (_docker build -t npoaudittrailviewer ._) and run the container (e.g. _docker run --name npo-audit-trail-viewer -v D:/git/nPOauditTrailViewer/tests/config.json:/var/www/html/config.json -d --restart=unless-stopped -p 8080:80 registry.gitlab.com/huwylphi/npoaudittrailviewer:latest_)
3. configure the parameters in the config.json file (with docker, you need to map a local config.json file with _/var/www/html/config.json_)
4. (configure audit trail in nPO)

## usage:
- generate web-report via the url `http://<host>/<virtual directory>/index.html`
- url parameters:
 - dynFilterList: e.g. dynFilterList=%5B"TagDescription%20LIKE%20%27%25UK3%25%27"%5D (like SQL filter but use url encoder. You can use an online url encoder like http://www.url-encode-decode.com/)
 - startDate and endDate: e.g. startDate=2015-06-01 or endDate=-7 in absolute or in relative format
 - autoStart: e.g. autoStart=true
 - pageLen: e.g. pageLen=50
 - colSort: e.g. colSort=-5 or colSort=-0. sort by col index
 - top: e.g. top=25. the top x items
- generate csv-export via command-line: `php -f exportCsv.php out=<exportFilePath> startDate=<start date in yyyy-mm-dd format> endDate=<end date in yyyy-mm-dd format>`
- generate predefine csv-export via command-line: `php -f reportLastMonth.php out=<exportFilePath>`
- or make your own predefine csv-export php script like in the `reportLastMonth.php` (you only have to calculate the startDate and endDate var)

**URL Parameters** (`http://<host-name or ip>/<virtual directory>/?<PARAM1&PARAM2>`):
- dynFilterList: e.g. `dynFilterList=%5B"TagDescription%20LIKE%20%27%25UK3%25%27"%5D` (like SQL filter but use url encoder. You can use an online url encoder like [url-encode-decode](http://www.url-encode-decode.com/))
- startDate and endDate: e.g. `startDate=2015-06-01` or `endDate=-7` in absolute or in relative format
- autoStart: e.g. `autoStart=true`
- pageLen: e.g. `pageLen=50`
- colSort: e.g. `colSort=-5` or `colSort=-0`. sort by col index
- top: ec. `top=25`. the top x items