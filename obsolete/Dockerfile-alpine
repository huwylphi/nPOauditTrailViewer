#FROM alpine:3.15
# Apache HTTP Server
#FROM httpd:2.4-alpine
FROM php:8.0.18-alpine3.15

RUN apk update && apk add autoconf make g++

# Install PHP
#RUN su
#RUN apk update && apk add php8 php8-dev php8-pear php8-pdo php8-openssl autoconf make g++
# The following symbolic links are optional but useful
#RUN ln -s /usr/bin/php8 /usr/bin/php
#RUN ln -s /usr/bin/phpize8 /usr/bin/phpize
#RUN ln -s /usr/bin/pecl8 /usr/bin/pecl
#RUN ln -s /usr/bin/php-config8 /usr/bin/php-config

# Install ODBC driver
RUN apk add gnupg curl

RUN curl -O https://download.microsoft.com/download/b/9/f/b9f3cce4-3925-46d4-9f46-da08869c6486/msodbcsql18_18.0.1.1-1_amd64.apk
RUN curl -O https://download.microsoft.com/download/b/9/f/b9f3cce4-3925-46d4-9f46-da08869c6486/mssql-tools18_18.0.1.1-1_amd64.apk

# (Optional) Verify signature
RUN curl -O https://download.microsoft.com/download/b/9/f/b9f3cce4-3925-46d4-9f46-da08869c6486/msodbcsql18_18.0.1.1-1_amd64.sig
RUN curl -O https://download.microsoft.com/download/b/9/f/b9f3cce4-3925-46d4-9f46-da08869c6486/mssql-tools18_18.0.1.1-1_amd64.sig

RUN curl https://packages.microsoft.com/keys/microsoft.asc | gpg --import -
RUN gpg --verify msodbcsql18_18.0.1.1-1_amd64.sig msodbcsql18_18.0.1.1-1_amd64.apk
RUN gpg --verify mssql-tools18_18.0.1.1-1_amd64.sig mssql-tools18_18.0.1.1-1_amd64.apk

# Install the packages
RUN apk add --allow-untrusted msodbcsql18_18.0.1.1-1_amd64.apk
RUN apk add --allow-untrusted mssql-tools18_18.0.1.1-1_amd64.apk

# Install the PHP drivers for Microsoft SQL Server 
RUN apk add unixodbc-dev

RUN pecl channel-update pecl.php.net

RUN pecl install sqlsrv
RUN pecl install pdo_sqlsrv
RUN su
RUN echo extension=pdo_sqlsrv.so >> `php --ini | grep "Scan for additional .ini files" | sed -e "s|.*:\s*||"`/10_pdo_sqlsrv.ini
RUN echo extension=sqlsrv.so >> `php --ini | grep "Scan for additional .ini files" | sed -e "s|.*:\s*||"`/20_sqlsrv.ini

# Use the default production configuration
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
RUN mv "/usr/local/etc/php/conf.d/10_pdo_sqlsrv.ini" "$PHP_INI_DIR/conf.d/"
RUN mv "/usr/local/etc/php/conf.d/20_sqlsrv.ini" "$PHP_INI_DIR/conf.d/"
RUN docker-php-ext-enable sqlsrv.so

# Install Apache and configure driver loading
RUN apk add php8-apache2 apache2
#RUN apk add php8-apache2

ENV APACHE_LOG_DIR /var/log/apache2

RUN rm -rf /var/cache/apk/*

# Install site
#COPY ./src/ /var/www/html/
COPY ./src/ /var/www/localhost/htdocs/
#COPY ./src/ /usr/local/apache2/htdocs/
#WORKDIR /var/www/html/
WORKDIR /var/www/localhost/htdocs/
#WORKDIR /usr/local/apache2/htdocs/

EXPOSE 80

# Start apache in the background as a service
CMD ["/usr/sbin/httpd", "-D", "FOREGROUND"]