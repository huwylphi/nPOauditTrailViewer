/* 
 * version: 1.6
 * date: 2017-01-13
 * developer: Ph. Huwyler
 
 * URL PARAMS:
 * - dynFilterList: ex. dynFilterList=%5B"TagDescription%20LIKE%20%27%25UK3%25%27"%5D (like SQL filter but use url encoder. You can use an online url encoder like http://www.url-encode-decode.com/)
 * - startDate and endDate: ex. startDate=2015-06-01 or endDate=-7 in absolute or in relative format
 * - autoStart: ex. autoStart=true
 * - pageLen: ex. pageLen=50
 * - colSort: ex. colSort=-5 or colSort=-0. sort by col index
 * - top: ec. top=25. the top x items
 */

// language selection
var set_locale_to = function(locale) {
	if (locale) {
		$.i18n().locale = locale;
	}

	$('body').i18n();

	if(!(locale === "en")) {
		$.getScript("inc/localization/datepicker-" + locale + ".js", function(data, textStatus, jqxhr) {
			$("#from, #to").datepicker("option", $.datepicker.regional[locale]);
		});
		$("#logo").attr("src", "inc/images/logo-" + locale + ".png");
	} else {
		$("#from, #to").datepicker("option", $.datepicker.regional[locale]);
		$("#from, #to").datepicker("option", "dateFormat", "dd/mm/yy");
		$("#logo").attr("src", "inc/images/logo.png");
	}
	$("button").click();
};

// load localization
var loadLocales = function(locales) {
	$.i18n().load(locales)
	.done(function() {
		var localeCount = 0;
		$.each(locales, function(locale) {
			localeCount++;
			var a = "<a href=\"#\" data-locale=\"" + locale + "\">" + locale.toUpperCase() + "</a>";
			if (localeCount < Object.keys(locales).length) {
				a = a + "&nbsp;|&nbsp;"
			}
			$(".switch-locale").append(a);
		});

		$('.switch-locale').on('click', 'a', function(e) {
			e.preventDefault();
			set_locale_to($(this).data('locale'));
		});
	});
}

$(document).ready(function() {
	// get all possible locales for localization
	$.getJSON("./getLocales.php?path=./inc/localization", loadLocales);

	$.getJSON("./config.json", function(config) {

		// helper function to get url params
		$.urlParam = function(name) {
			var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
			if (results==null) {
				return null;
			} else {
				return decodeURIComponent(results[1]) || 0;
			}
		}

		// first hide table and restore it after the query
		$("#tbl").hide();
		var $loading = $('#spinner').hide();
		
		// init from - to field as datepicker
		$("#from").datepicker({
			defaultDate: "-1w",
			changeMonth: true,
			numberOfMonths: 3,
			onClose: function( selectedDate ) {
				$("#to").datepicker("option", "minDate", selectedDate);
			}
		});
		$("#to").datepicker({
			defaultDate: "+1w",
			changeMonth: true,
			numberOfMonths: 3,
			onClose: function( selectedDate ) {
				$("#from").datepicker("option", "maxDate", selectedDate);
			}
		});
		// format datepicker
		$("#from, #to").datepicker("option", $.datepicker.regional["en"]);
		$("#from, #to").datepicker("option", "dateFormat", "dd/mm/yy");
		
		// init datepicker with date of today or from url params startDate and endDate (in absolute or relative format)
		$("#to").datepicker('setDate', new Date());
		if($.urlParam('endDate') != null) {
			if((new RegExp(/^\-{1}\d+$/)).test($.urlParam('endDate')) == true) {
				var d = new Date();
				d.setDate(d.getDate()-Math.abs(parseInt($.urlParam('endDate'))));
				$( "#to" ).datepicker('setDate', d);
			} else if((new RegExp(/\b\d{4}[-]\d{1,2}[-]\d{1,2}\b/)).test($.urlParam('endDate')) == true) {
				$("#to").datepicker('setDate', new Date($.urlParam('endDate')));
			}
		}
		
		$("#from").datepicker('setDate', new Date());
		if($.urlParam('startDate') != null) {
			if((new RegExp(/^\-{1}\d+$/)).test($.urlParam('startDate')) == true) {
				var d = new Date();
				d.setDate(d.getDate()-Math.abs(parseInt($.urlParam('startDate'))));
				$("#from").datepicker('setDate', d);
			} else if((new RegExp(/\b\d{4}[-]\d{1,2}[-]\d{1,2}\b/)).test($.urlParam('startDate')) == true) {
				$("#from").datepicker('setDate', new Date($.urlParam('startDate')));
			}
		} else if($.urlParam('top') != null) {	// case startDate not defined and top is defined, set startDate to first possible date (otherwise startDate would be today or given params)
			$("#from").datepicker('setDate', new Date('1970-01-01'));
		}

		// get autoStart param from url to check if we need to run the query on site loading
		var autoStart = false;
		if($.urlParam('autoStart') != null) {
			try {
				if($.parseJSON($.urlParam('autoStart')) === true) {
					autoStart = true;
				}
			} catch (err) {throw err;}
		}
		
		// query and display table on click
		var table;
		$("button").button()
		.click(function(event) {
			// display spinner
			$loading.show();
			
			// show table
			if (typeof table !== 'undefined') {
				table.clear();
				
				table.destroy();
				$('#tbl').empty(); // empty in case the columns change
				
				$('#tbl tr').remove();
				$('#tbl').hide();
				$('#tbl_wrapper').hide();
			}

			$('#tblCaption').text(">  ...");

			var f = $.datepicker.formatDate(config.sqlServer.dateFormat, new Date($("#from").datepicker("getDate")));
			var t = $.datepicker.formatDate(config.sqlServer.dateFormat, new Date($("#to").datepicker("getDate")));

			var req = './getData.php?';
			if($("#from").val()) {
				req = req + 'startDate=' + f;
			}
			if($("#to").val()) {
				req = req + '&endDate=' + t;
			}
			
			if($.urlParam('dynFilterList') != null) {
				req = req + "&dynFilterList=" + $.urlParam('dynFilterList');
			}
			
			if($.urlParam('top') != null) {
				req = req + "&top=" + $.urlParam('top');
			}

			if(config.debug) {
				console.log("getData.php ajax request:" + req);
			}

			var ATtblColumnNames4DataTable = [];
			$.ajax({
				type: "POST",
				dataType: "json",
				url: req,
				timeout: config.ajax.timeout,
				error: function(xhr, ajaxOptions, thrownError) {
					// will fire when timeout is reached
					$('#tblCaption').text("> " + thrownError + " (" + xhr.status + ")");
					$loading.hide();
				},
				success: function(json) {
					// set table caption
					var opt = {
						dayNamesShort: $.datepicker.regional[$.i18n().locale].dayNamesShort,
						dayNames: $.datepicker.regional[$.i18n().locale].dayNames,
						monthNamesShort: $.datepicker.regional[$.i18n().locale].monthNamesShort,
						monthNames: $.datepicker.regional[$.i18n().locale].monthNames
					}
					var caption	= $.datepicker.formatDate("D dd. MM yy", new Date(json.period.from.year, json.period.from.month-1, json.period.from.day), opt)
								+ " - "
								+ $.datepicker.formatDate("D dd. MM yy", new Date(json.period.to.year, json.period.to.month-1, json.period.to.day), opt);

					$('#tbl').find("caption")
					.text(caption)	// set table caption so it will be printable
					.hide();		// but hide the original table caption because it will be set in #tblCaption
					$('#tblCaption').text(">  " + caption);
					
					// get header
					
					//$("#tbl thead").remove();
					//var thead = $('<thead></thead>').appendTo('#tbl');
					var tr = $('<tr></tr>').appendTo($('#tblh'));
					
					if(json.data.length > 0) {
						// generate columns param for DataTable object as array
						$.each(json.data[0], function(a, b) {
							var colCaption = $.i18n('dataTable-columnCaption-' + a);
							colCaption = colCaption.replace('dataTable-columnCaption-', '');
							ATtblColumnNames4DataTable.push($.parseJSON('{"data": "' + a + '", "title":"' + colCaption + '"}'));
							$('<th>' + a + '</th>').appendTo(tr);
						});

						$.each(json.data, function(a, b) {
							var tr = $('<tr></tr>').appendTo($('#tblb'));
							$.each(b, function(i, j) {
								$('<td>' + j + '</td>').appendTo(tr);
							});
						});

						// generate table with new DataTable http://datatables.net/
						table = $('#tbl')
						.on('init.dt', function () {	// init table depending on url params
							// col re-order
							/*
							var nbCol = table.columns().header().length;
							var configTableColReorder = config.table.colReorder;
							$.each(config.table.colReorder, function(a, b) {
								if(b >= nbCol) {
									configTableColReorder.remove(a);
								}
							});
							table.colReorder.order(config.table.colReorder, true);
							*/
							// col visibility
							/*
							$.each(config.table.colHidden, function(a, b) {
								// Toggle the visibility
								table.column(b).visible(false);
								//table.column('#column-'+b).visible(false); unavailable... use index instead of id
							});
							*/
							// page len
							if($.urlParam('pageLen') != null) {
								try {
									table.page.len(parseInt($.urlParam('pageLen'))).draw();
								} catch (err) {}
							}
							
							// col sort from url
							if($.urlParam('colSort') != null) {
								try {
									table.order([Math.abs(parseInt($.urlParam('colSort'))), $.urlParam('colSort').indexOf('-') > -1 ? 'desc' : 'asc']).draw();
								} catch (err) {}
							}
							
							$loading.hide();
							$('#tbl').show();
						})
						.DataTable({
							//data: json.data
							//columns: ATtblColumnNames4DataTable
							//destroy: true,
							//fixedHeader: true,
							//responsive: true,
							//deferRender: true,
							//scroller: true,
							scrollY: 200,
							scrollX: true
							//autoWidth: true,
							//stateSave: true,
							//lengthMenu: [[10, 20, 50, 100, -1], [10, 20, 50, 100, $.i18n('dataTable-all')]],
							//paging: true,
							//pagingType: "full_numbers",
							//colReorder: true,
							/*columnDefs: [{
								className: "dt-head-left",
								targets: [ 10 ]
							}],*/
							//dom: '<"dth1"<"dtl"l><"dtf"f><"dtB"B>><"dth2"<"dt"t><"dti"i><"dtp"p>>',
							/*buttons: [
								{
									extend: 'colvis',
									text: $.i18n('dataTable-buttons-colvis-text')
								},
								{
									extend: 'print',
									title: $.i18n('app-title'),
									text: $.i18n('dataTable-buttons-print-text')
								},
								{
									extend: 'csv',
									text: 'CSV'
								},
								{
									extend: 'copy',
									text: $.i18n('dataTable-buttons-copy-text')
								},
								{
									extend: 'excel',
									text: 'EXCEL'
								},
								{
									extend: 'pdf',
									title: $.i18n('app-title'),
									text: 'PDF'
								}
							],
							language: {
								url: "./inc/localization/" + $.i18n().locale + ".json",
							},
							"initComplete": function(settings, json) {
								$("<br /><br />").insertAfter(".dth1");
							}*/
						});
					} else {
						// no data 
						$('#tblCaption').text("> " + $.i18n('nodata'));
						$loading.hide();
					}
				}
			});
		});
		event.preventDefault();

		// auto start query if autoStart param is true
		if(autoStart) {
			$("button").click();
		}
	})
	.fail(function() {
		console.log("Error loading config file! Please check config.json");
	});
});