# PHP with Apache HTTP Server
FROM php:8.1-apache

# Install ODBC driver for Debian 11
RUN apt-get update && apt-get install -y gnupg
RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
RUN curl https://packages.microsoft.com/config/debian/11/prod.list > /etc/apt/sources.list.d/mssql-release.list

RUN apt-get update
ENV ACCEPT_EULA Y
RUN apt-get install -y msodbcsql18
# optional: for bcp and sqlcmd
RUN apt-get install -y mssql-tools18
RUN echo 'export PATH="$PATH:/opt/mssql-tools18/bin"' >> ~/.bashrc
RUN . ~/.bashrc
# optional: for unixODBC development headers
RUN apt-get install -y unixodbc-dev

# Install the PHP drivers for Microsoft SQL Server 
RUN pecl install sqlsrv
#RUN pecl install pdo_sqlsrv

# Use the production PHP configuration and add sqlsrv extensions
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
RUN printf "; priority=20\nextension=sqlsrv.so\n" > "$PHP_INI_DIR/conf.d/sqlsrv.ini"
#RUN printf "; priority=30\nextension=pdo_sqlsrv.so\n" > "$PHP_INI_DIR/conf.d/pdo_sqlsrv.ini"
RUN docker-php-ext-enable sqlsrv.so

# Configure Apache
ENV APACHE_LOG_DIR /var/log/apache2

# Install site
COPY ./src/ /var/www/html/
WORKDIR /var/www/html/
EXPOSE 80